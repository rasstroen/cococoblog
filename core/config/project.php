<?php
/**
 * @var string $rootPath
 */
return [
    'routing' => [
        'rules' => [
            '' => \Blog\Controller\Entry\Entries::class, // entries list
            'login' => \Blog\Controller\Login::class, // login page
            'entries' => [
                '%d=entryId' =>
                    [
                        '' => \Blog\Controller\Entry\Entry::class,
                        'edit' => \Blog\Controller\Entry\Edit::class // entry add page
                    ],
                'post' => \Blog\Controller\Entry\Post::class,// entry add page

            ]
        ]
    ],
    'path' => [
        'templates' => $rootPath . 'templates' . DIRECTORY_SEPARATOR
    ],
    'mysql' => [
        'host' => 'localhost',
        'user' => 'root',
        'password' => '2912',
        'database' => 'cococo'
    ]
];
