<?php
spl_autoload_register(function ($class) use ($rootPath) {
    $fileName = $rootPath . str_replace('\\', '/', $class) . '.php';
    if (is_readable($fileName)) {
        require $fileName;
    }
});
