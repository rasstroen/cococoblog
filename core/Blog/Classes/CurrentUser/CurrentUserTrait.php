<?php namespace Blog\Classes\CurrentUser;


trait CurrentUserTrait
{
    /**
     * @return CurrentUser
     */
    protected function getCurrentUser()
    {
        return CurrentUser::instance();
    }
}