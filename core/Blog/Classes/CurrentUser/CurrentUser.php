<?php namespace Blog\Classes\CurrentUser;

use Blog\Classes\User\User;
use Blog\Classes\User\UserServiceTrait;
use Blog\InjectableTrait;
use Blog\Request\RequestTrait;

class CurrentUser
{
    use InjectableTrait;
    use RequestTrait;
    use UserServiceTrait;

    /**
     * @var User
     */
    private $user;

    /**
     * @var bool
     */
    private $loaded;

    /**
     * @var int
     */
    private $userId;

    /**
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
        $this->getRequest()->setSessionParam('uid', $userId);
    }

    /**
     * @return bool
     */
    public function logout()
    {
        $this->userId = null;
        $this->loaded = false;
        $this->getRequest()->setSessionParam('uid', null);
        return true;
    }


    public function loadUser()
    {
        if (null === $this->loaded) {
            $currentUserId = (int) $this->getRequest()->getSessionParam('uid');
            if ($currentUserId) {
                $this->user = $this->getUserService()->getById($currentUserId);
                if($this->user) {
                    $this->setUserId($this->user->getId());
                }
            }
            $this->loaded = true;
        }
    }

    public function isAuthorized()
    {
        $this->loadUser();
        return $this->user !== null;
    }

    public function getUser()
    {
        return $this->user;
    }
}
