<?php namespace Blog\Classes\Entity;

abstract class Base
{
    /**
     * Entry constructor.
     * @param array|null $data
     */
    public function __construct(array $data = null)
    {
        if (!empty($data)) {
            foreach ($this->getMeta() as $propertyName => $tableField) {
                if (isset($data[$tableField])) {
                    $setter = 'set' . $propertyName;
                    $this->$setter($data[$tableField]);
                }
            }
        }
    }

    abstract protected function getMeta():array;
}
