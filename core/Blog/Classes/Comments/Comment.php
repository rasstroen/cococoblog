<?php namespace Blog\Classes\Comments;

use Blog\Classes\Entity\Base;
use Blog\Classes\User\User;

class Comment extends Base
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $entryId;

    /**
     * @var int
     */
    private $time;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $text;

    /**
     * @var int
     */
    private $authorId;

    /**
     * @var User
     */
    private $author;

    /**
     * @return array
     */
    protected function getMeta():array
    {
        return [
            'id' => 'id',
            'entryId' => 'entry_id',
            'name' => 'name',
            'text' => 'text',
            'authorId' => 'user_id',
            'time' => 'time',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getEntryId()
    {
        return $this->entryId;
    }

    /**
     * @param int $entryId
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getFormattedDate()
    {
        return date('d.m.Y H:i', $this->getTime());
    }
}
