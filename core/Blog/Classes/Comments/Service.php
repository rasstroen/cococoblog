<?php namespace Blog\Classes\Comments;

use Blog\Classes\Paging;
use Blog\Classes\User\UserServiceTrait;
use Blog\Database\DbConnectionTrait;
use Blog\InjectableTrait;

class Service
{
    use InjectableTrait;
    use DbConnectionTrait;
    use UserServiceTrait;

    /**
     * @param int $entryId
     * @param int $userId
     * @param string $text
     * @param string $name
     * @return bool|\mysqli_result
     * @throws \Exception
     */
    public function addComment(int $entryId, int $userId, string $text, string $name)
    {
        $connection = $this->getDb()->getConnection();
        $connection->query(
            'INSERT INTO `comments` SET
              `time` = unix_timestamp(),
              `entry_id`="' . $connection->escape($entryId) . '",
              `user_id`="' . $connection->escape($userId) . '",
              `text`="' . $connection->escape($text) . '",
              `name`="' . $connection->escape($name) . '"
            '
        );
        return $connection->lastInsertId();
    }

    /**
     * @param int $entryId
     * @return int
     */
    public function getEntryCommentsCount(int $entryId)
    {
        return (int) $this->getDb()->getConnection()->selectSingle('SELECT COUNT(1) FROM `comments` WHERE `entry_id`=' . $entryId);
    }

    /**
     * @param array $comments
     */
    private function addEntriesUsers(array &$comments)
    {
        /**
         * @var Comment[] $comments
         */
        $authorIds = [];
        foreach ($comments as $comment) {
            $authorIds[] = $comment->getAuthorId();
        }

        if (!empty($authorIds)) {
            $users = $this->getUserService()->getByIds($authorIds);
            foreach ($comments as $comment) {
                if (isset($users[$comment->getAuthorId()])) {
                    $comment->setAuthor($users[$comment->getAuthorId()]);
                }
            }
        }
    }

    /**
     * @param Paging $paging
     * @param int $entryId
     * @param int|null $totalCount
     * @return array
     * @throws \Exception
     */
    public function getWithPaging(Paging $paging, int $entryId, int &$totalCount = null)
    {
        $connection = $this->getDb()->getConnection();
        $limit = $paging->getLimit();
        $rows = $connection->selectAll('SELECT SQL_CALC_FOUND_ROWS * FROM `comments` WHERE `entry_id`=' . $entryId . ' ORDER BY `time` ' . $limit);
        if (null !== $totalCount) {
            $totalCount = $connection->selectSingle('SELECT FOUND_ROWS()');
        }

        $entries = [];
        foreach ($rows as $row) {
            $entries[$row['id']] = new Comment($row);
        }
        $this->addEntriesUsers($entries);

        return $entries;
    }
}