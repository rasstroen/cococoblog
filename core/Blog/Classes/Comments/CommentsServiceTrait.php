<?php namespace Blog\Classes\Comments;

trait CommentsServiceTrait
{
    /**
     * @return Service
     */
    protected function getCommentsService()
    {
        return Service::instance();
    }
}
