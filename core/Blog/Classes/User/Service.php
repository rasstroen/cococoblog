<?php namespace Blog\Classes\User;

use Blog\Database\DbConnectionTrait;
use Blog\InjectableTrait;

class Service
{
    use InjectableTrait;
    use DbConnectionTrait;

    /**
     * @param array $userIds
     * @return User[]
     */
    public function getByIds(array $userIds)
    {
        $result = [];
        $connection = $this->getDb()->getConnection();
        $rows = $this->getDb()->getConnection()->selectAll('SELECT * FROM `user` WHERE `id` IN (' . $connection->escape(implode(',',
                $userIds)) . ')');
        foreach ($rows as $row) {
            $result[$row['id']] = new User($row);
        }

        return $result;
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function getById(int $userId)
    {
        $users = $this->getByIds([$userId]);
        return !empty($users) ? reset($users) : null;
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function getByUsername(string $username)
    {
        $connection = $this->getDb()->getConnection();
        $row = $this->getDb()->getConnection()->selectRow('SELECT * FROM `user` WHERE `username` = "' . $connection->escape($username) . '"');
        if ($row) {
            return new User($row);
        }
        return null;
    }

    /**
     * @param string $password
     * @return string
     */
    public function getPasswordHash(string $password):string
    {
        return md5($password);
    }

    /**
     * @param string $username
     * @param string $password
     * @return User|null
     * @throws \Exception
     */
    public function createUser(string $username, string $password)
    {
        $user = null;
        $passwordHash = $this->getPasswordHash($password);
        $connection = $this->getDb()->getConnection();
        $success = $connection->query('INSERT INTO `user`(`username`,`password_hash`) VALUES("' . $connection->escape($username) . '","' . $connection->escape($passwordHash) . '")');
        if ($success) {
            $user = $this->getByUsername($username);
        }
        return $user;
    }
}