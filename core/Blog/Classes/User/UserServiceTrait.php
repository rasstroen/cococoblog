<?php namespace Blog\Classes\User;


trait UserServiceTrait
{
    /**
     * @return Service
     */
    protected function getUserService()
    {
        return Service::instance();
    }
}
