<?php namespace Blog\Classes\Entries;

use Blog\Classes\Paging;
use Blog\Classes\User\UserServiceTrait;
use Blog\Database\DbConnectionTrait;
use Blog\InjectableTrait;

class Service
{
    use InjectableTrait;
    use DbConnectionTrait;
    use UserServiceTrait;

    /**
     * @param Paging $paging
     * @param int|null $totalCount
     * @return array
     * @throws \Exception
     */
    public function getWithPaging(Paging $paging, int &$totalCount = null)
    {
        $connection = $this->getDb()->getConnection();
        $limit = $paging->getLimit();
        $rows = $connection->selectAll('SELECT SQL_CALC_FOUND_ROWS * FROM `entries` ORDER BY `time` DESC ' . $limit);
        if (null !== $totalCount) {
            $totalCount = $connection->selectSingle('SELECT FOUND_ROWS()');
        }

        $entries = [];
        foreach ($rows as $row) {
            $entries[$row['id']] = new Entry($row);
        }
        $this->addAuthors($entries);
        $this->addCommentsCounters($entries);

        return $entries;
    }

    /**
     * @param string $title
     * @param string $text
     * @param int $userId
     * @return mixed
     * @throws \Exception
     */
    public function addEntry(string $title, string $text, int $userId)
    {
        $connection = $this->getDb()->getConnection();
        $connection->query(
            'INSERT INTO `entries` SET 
              `title`="' . $connection->escape($title) . '",
              `text`="' . $connection->escape($text) . '",
              `author_id`="' . $connection->escape($userId) . '",
              `time`=unix_timestamp()'
        );
        return $connection->lastInsertId();
    }

    /**
     * @param int $entryId
     * @param string $title
     * @param string $text
     * @return bool|\mysqli_result
     * @throws \Exception
     */
    public function updateEntry(int $entryId, string $title, string $text)
    {
        $connection = $this->getDb()->getConnection();
        return $connection->query(
            'UPDATE `entries` SET 
              `title`="' . $connection->escape($title) . '",
              `text`="' . $connection->escape($text) . '"
              WHERE `id`=' . $connection->escape($entryId)
        );
    }

    /**
     * @param int $entryId
     * @return bool|\mysqli_result
     * @throws \Exception
     */
    public function deleteEntry(int $entryId)
    {
        $connection = $this->getDb()->getConnection();
        $connection->query('START TRANSACTION');
        $connection->query('DELETE FROM `entries` WHERE `id`=' . $connection->escape($entryId));
        $connection->query('DELETE FROM `comments` WHERE `entry_id`=' . $connection->escape($entryId));
        return $connection->query('COMMIT');
    }

    /**
     * @param array $entries
     */
    private function addAuthors(array &$entries)
    {
        $authorIds = [];
        foreach ($entries as $entry) {
            $authorIds[] = $entry->getAuthorId();
        }

        if (!empty($authorIds)) {
            $users = $this->getUserService()->getByIds($authorIds);
            foreach ($entries as $entry) {
                if (isset($users[$entry->getAuthorId()])) {
                    $entry->setAuthor($users[$entry->getAuthorId()]);
                }
            }
        }
    }


    /**
     * @param Entry[] $entries
     * @return Entry[]
     * @throws \Exception
     */
    public function addCommentsCounters(array &$entries)
    {
        $entryIds = array_keys($entries);
        if (empty($entryIds)) {
            return [];
        }
        $connection = $this->getDb()->getConnection();
        $rows = $connection->selectAll(
            'SELECT 
              entry_id, COUNT(1) as `comments_count` 
              FROM 
                `comments` 
              WHERE 
                `entry_id` IN(' . $connection->escape(implode(',', $entryIds)) . ')
              GROUP BY entry_id'
        );
        foreach ($rows as $row) {
            $entries[$row['entry_id']]->setCommentsCount((int) $row['comments_count']);
        }
    }


    /**
     * @param int $entryId
     * @return Entry|null
     */
    public function getById(int $entryId)
    {
        $entries = $this->getByIds([$entryId]);
        return $entries[$entryId] ?? null;
    }

    public function getByIds(array $entryIds)
    {
        $connection = $this->getDb()->getConnection();
        $rows = $this->getDb()->getConnection()->selectAll(
            'SELECT * FROM `entries` WHERE `id` IN(' . $connection->escape(implode(',', $entryIds)) . ')'
        );
        $entries = [];
        foreach ($rows as $row) {
            $entries[$row['id']] = new Entry($row);
        }
        /**
         * @var Entry[] $entries
         */
        $this->addAuthors($entries);
        return $entries;
    }
}