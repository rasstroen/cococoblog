<?php namespace Blog\Classes\Entries;


trait EntriesServiceTrait
{
    /**
     * @return Service
     */
    protected function getEntriesService()
    {
        return Service::instance();
    }
}
