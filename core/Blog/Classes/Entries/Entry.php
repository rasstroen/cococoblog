<?php namespace Blog\Classes\Entries;

use Blog\Classes\Entity\Base;
use Blog\Classes\User\User;

class Entry extends Base
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $time;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var int
     */
    private $authorId;

    /**
     * @var User
     */
    private $author;

    /**
     * @var int
     */
    private $commentsCount;

    /**
     * @return array
     */
    protected function getMeta():array
    {
        return [
            'id' => 'id',
            'title' => 'title',
            'text' => 'text',
            'authorId' => 'author_id',
            'time' => 'time',
        ];
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return '/entries/' . $this->getId();
    }

    /**
     * @return string
     */
    public function getCommentsUrl()
    {
        return '/entries/' . $this->getId() . '#comments';
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return '/entries/' . $this->getId() . '/edit';
    }

    /**
     * @param int $wordsCount
     * @return mixed
     */
    public function getShortText(int $wordsCount)
    {
        $text = html_entity_decode(str_replace('&nbsp;', ' ', $this->getText()), ENT_QUOTES, 'UTF-8');
        $text = str_replace(array('<br>', '<br />', '<br/>'), ' ', $text);
        $noHtml = trim(strip_tags($text));
        $exploded = explode(' ', $noHtml, ($wordsCount + 1));
        if (count($exploded) > $wordsCount) {
            array_pop($exploded);
            $exploded[] = '...';
        }
        mb_regex_encoding('UTF-8');
        return mb_ereg_replace('/ {2,}/', 's', implode(' ', $exploded));
    }

    public function getFormattedDate()
    {
        return date('d.m.Y H:i', $this->getTime());
    }

    /**
     * @return int
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * @param int $commentsCount
     */
    public function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;
    }
}
