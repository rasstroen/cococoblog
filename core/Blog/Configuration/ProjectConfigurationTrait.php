<?php namespace Blog\Configuration;

trait ProjectConfigurationTrait
{
    /**
     * @return Project
     */
    protected function getProjectConfiguration()
    {
        return Project::instance();
    }
}
