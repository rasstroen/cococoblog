<?php namespace Blog\Configuration;

use Blog\InjectableTrait;

class Project
{
    use InjectableTrait;

    /**
     * @var array
     */
    private $configuration;

    /**
     * @param array $configuration
     */
    public function setConfiguration(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $settingPath
     * @param null $defaultValue
     * @return array|mixed|null
     */
    public function get(string $settingPath, $defaultValue = null)
    {
        $settingPath = explode('.', $settingPath);
        $configuration = $this->configuration;
        while ($pathPart = array_shift($settingPath)) {
            if (isset($configuration[$pathPart])) {
                $configuration = $configuration[$pathPart];
            } else {
                return $defaultValue;
            }
        }
        return $configuration;
    }
}
