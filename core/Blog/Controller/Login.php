<?php namespace Blog\Controller;

use Blog\Classes\User\UserServiceTrait;
use Blog\Controller\Exception\IllegalArgument;

class Login extends Dynamic
{
    use UserServiceTrait;

    protected function execute():array
    {
        $response = [];
        return $response;
    }

    protected function getLayout():string
    {
        return 'login';
    }

    protected function getTitle():string
    {
        return 'Blog - login page';
    }

    protected function executeWriteAction(string $writeActionName)
    {
        switch ($writeActionName) {
            case 'register':
                return $this->actionRegister();
                break;
            case 'logout':
                return $this->actionLogout();
                break;
            case 'login':
                return $this->actionLogin();
                break;
        }
    }

    /**
     * @param $username
     * @param $password
     * @return array
     * @throws IllegalArgument
     */
    private function getLoginParams(&$username, &$password)
    {
        $username = trim((string) $this->getRequest()->getPostParam('username'));
        $password = trim((string) $this->getRequest()->getPostParam('password'));
        $errors = [];
        if (!$username) {
            $errors[] = 'illegal login';
        }
        if (!$password) {
            $errors[] = 'illegal password';
        }
        if (mb_strlen($username) > 100) {
            $errors[] = 'too long login (100 symbols max)';
        }
        if (mb_strlen($password) > 100) {
            $errors[] = 'too long password (100 symbols max)';
        }
        return $errors;
    }

    /**
     * @return array
     */
    private function actionRegister()
    {
        $errors = $this->getLoginParams($username, $password);
        if (!empty($errors)) {
            return [
                'errors' => $errors
            ];
        }
        $user = $this->getUserService()->getByUsername($username);
        if (null === $user) {
            $user = $this->getUserService()->createUser($username, $password);
            $this->getCurrentUser()->setUserId($user->getId());
        } else {
            return [
                'errors' => ['user with this username is already registered'],
                'username' => $username
            ];
        }
    }

    /**
     * @return array
     */
    private function actionLogin()
    {
        $errors = $this->getLoginParams($username, $password);
        if (!empty($errors)) {
            return [
                'errors' => $errors
            ];
        }
        $user = $this->getUserService()->getByUsername($username);
        if (!$user) {
            return [
                'errors' => ['user with this username is not registered'],
                'username' => $username
            ];
        }
        $passwordHash = $this->getUserService()->getPasswordHash($password);
        if ($passwordHash === $user->getPasswordHash()) {
            $this->getCurrentUser()->setUserId($user->getId());
        } else {
            return [
                'errors' => ['invalid password for user'],
                'username' => $username
            ];
        }
    }

    /**
     * @return bool
     */
    private function actionLogout()
    {
        return $this->getCurrentUser()->logout();
    }
}
