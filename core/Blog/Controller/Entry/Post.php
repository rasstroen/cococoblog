<?php namespace Blog\Controller\Entry;

use Blog\Classes\Entries\EntriesServiceTrait;
use Blog\Classes\Entries\Entry;
use Blog\Controller\Dynamic;
use Blog\Request\RequestTrait;
use Blog\Router\Exception\Access;
use Blog\Router\Exception\NotFound;

class Post extends Dynamic
{
    use EntriesServiceTrait;
    use RequestTrait;

    /**
     * @return string
     */
    protected function getLayout():string
    {
        return 'post';
    }

    /**
     * @throws Access
     */
    protected function beforeAction()
    {
        if (!$this->getCurrentUser()->isAuthorized()) {
            throw new Access('unauthorized user');
        }
        parent::beforeAction();
    }

    /**
     * @return string
     */
    protected function getTitle():string
    {
        $entryId = (int) $this->getVariable('entryId');
        if ($entryId) {
            $entry = $this->getEntriesService()->getById($entryId);
            return $entry->getTitle();
        }
        return '';
    }

    /**
     * @return array
     */
    protected function execute():array
    {
        $response = [];
        return $response;
    }

    /**
     * @param string $title
     * @param string $text
     * @return array
     */
    private function checkPostParams(string $title, string $text)
    {
        $errors = [];
        if (mb_strlen($title) < 1) {
            $errors['title'] = 'too short title';
        }

        if (mb_strlen($text) < 1) {
            $errors['text'] = 'too short text';
        }
        return $errors;
    }

    /**
     * @throws Access
     * @throws NotFound
     * @throws \Exception
     */
    private function actionDelete()
    {
        if (!$this->getCurrentUser()->isAuthorized()) {
            throw new Access('unauthorized user');
        }

        $entryId = (int) $this->getRequest()->getPostParam('id');

        if (!$entryId) {
            throw new \Exception('illegal entry id');
        }
        $entry = $this->getEntriesService()->getById($entryId);

        if (!$entry) {
            throw new NotFound();
        }

        if ($this->getCurrentUser()->getUser()->getId() != $entry->getAuthorId()) {
            throw new Access('illegal user');
        }
        $this->getEntriesService()->deleteEntry($entry->getId());
        $this->getRequest()->redirect('/');
    }

    /**
     * @return array
     * @throws Access
     * @throws NotFound
     */
    private function actionPost()
    {
        if (!$this->getCurrentUser()->isAuthorized()) {
            throw new Access('unauthorized user');
        }
        $title = trim((string) $this->getRequest()->getPostParam('title'));
        $text = (string) $this->getRequest()->getPostParam('text');
        $text = trim($text);
        $text = strip_tags($text);
        $entryId = (int) $this->getRequest()->getPostParam('id');

        $entry = null;
        if ($entryId) {
            $entry = $this->getEntriesService()->getById($entryId);

            if (!$entry) {
                throw new NotFound();
            }

            if ($this->getCurrentUser()->getUser()->getId() != $entry->getAuthorId()) {
                throw new Access('illegal user');
            }
        }

        $errors = $this->checkPostParams($title, $text);

        if (!count($errors)) {
            if (!$entry) {
                $entryId = $this->getEntriesService()->addEntry(
                    $title,
                    $text,
                    $this->getCurrentUser()->getUser()->getId()
                );
                $entry = new Entry();
                $entry->setId($entryId);
            } else {
                $this->getEntriesService()->updateEntry($entryId, $title, $text);
            }
            $this->getRequest()->redirect($entry->getUrl());
        }

        return [
            'errors' => $errors,
            'title' => $title,
            'text' => $text
        ];
    }

    /**
     * @param string $writeActionName
     * @return array|void
     * @throws Access
     * @throws NotFound
     * @throws \Exception
     */
    protected function executeWriteAction(string $writeActionName)
    {
        switch ($writeActionName) {
            case 'post':
                return $this->actionPost();
                break;
            case 'delete':
                return $this->actionDelete();
                break;
        }
    }
}