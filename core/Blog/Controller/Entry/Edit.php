<?php namespace Blog\Controller\Entry;

use Blog\Router\Exception\Access;
use Blog\Router\Exception\NotFound;

class Edit extends Post
{
    protected function execute():array
    {
        if (!$this->getCurrentUser()->isAuthorized()) {
            throw new Access('unauthorized user');
        }

        $entryId = (int) $this->getVariable('entryId');
        if (!$entryId) {
            throw new NotFound();
        }

        $entry = $this->getEntriesService()->getById($entryId);
        if (!$entry) {
            throw new NotFound();
        }

        if ($this->getCurrentUser()->getUser()->getId() != $entry->getAuthorId()) {
            throw new Access('illegal user');
        }
        $response = [
            'entry' => $entry
        ];
        return $response;
    }
}
