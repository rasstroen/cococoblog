<?php namespace Blog\Controller\Entry;

use Blog\Classes\Comments\CommentsServiceTrait;
use Blog\Classes\Entries\EntriesServiceTrait;
use Blog\Classes\Paging;
use Blog\Controller\Dynamic;
use Blog\Request\RequestTrait;
use Blog\Router\Exception\NotFound;

class Entry extends Dynamic
{
    const COMMENTS_PER_PAGE = 10;

    use EntriesServiceTrait;
    use RequestTrait;
    use CommentsServiceTrait;

    /**
     * @return string
     */
    protected function getLayout():string
    {
        return 'entry';
    }

    /**
     * @return string
     */
    protected function getTitle():string
    {
        $entryId = (int) $this->getVariable('entryId');
        if ($entryId) {
            $entry = $this->getEntriesService()->getById($entryId);
            return $entry->getTitle();
        }
        return '';
    }

    /**
     * @return array
     * @throws NotFound
     */
    protected function execute():array
    {
        $entryId = (int) $this->getVariable('entryId');
        if (!$entryId) {
            throw new NotFound();
        }
        $entry = $this->getEntriesService()->getById($entryId);

        if (!$entry) {
            throw new NotFound();
        }


        $currentPage = max(1, $this->getRequest()->getQueryParam('page', 1));
        $perPage = self::COMMENTS_PER_PAGE;
        $totalCount = 0;

        $paging = new Paging();
        $paging->setPage($currentPage);
        $paging->setPerPage($perPage);

        $comments = $this->getCommentsService()->getWithPaging($paging, $entry->getId(), $totalCount);
        $paging->setTotalItemsCount($totalCount);

        $response = [
            'commentId' => (int) $this->getRequest()->getQueryParam('commentId'),
            'entry' => $entry,
            'comments' => $comments,
            'paging' => $paging
        ];
        return $response;
    }

    /**
     * @param string $name
     * @param string $text
     * @return array
     */
    private function checkCommentParams(string $name, string $text)
    {
        $errors = [];
        $nameLength = mb_strlen($name);
        $textLength = mb_strlen($text);
        if ($nameLength < 1) {
            $errors['title'] = 'too short name';
        }

        if ($textLength < 1) {
            $errors['text'] = 'too short text';
        }

        if ($nameLength > 100) {
            $errors['title'] = 'too long name';
        }

        if ($textLength > 1000) {
            $errors['text'] = 'too long text';
        }
        return $errors;
    }

    /**
     * @return array
     * @throws NotFound
     */
    private function postComment()
    {

        $entryId = (int) $this->getVariable('entryId');
        if (!$entryId) {
            throw new NotFound();
        }
        $entry = $this->getEntriesService()->getById($entryId);

        if (!$entry) {
            throw new NotFound();
        }

        $name = trim((string) $this->getRequest()->getPostParam('name'));
        $text = (string) $this->getRequest()->getPostParam('text');
        $text = trim($text);
        $text = strip_tags($text);

        $userId = 0;
        if ($this->getCurrentUser()->isAuthorized()) {
            $name = $this->getCurrentUser()->getUser()->getUsername();
            $userId = $this->getCurrentUser()->getUser()->getId();
        }

        $errors = $this->checkCommentParams($name, $text);

        if (empty($errors)) {

            $commentId = $this->getCommentsService()->addComment($entry->getId(), $userId, $text, $name);
            $commentsCount = $this->getCommentsService()->getEntryCommentsCount($entry->getId());
            $lastPage = ceil($commentsCount / self::COMMENTS_PER_PAGE);
            
            $this->getRequest()->redirect($entry->getUrl() . '?page=' . $lastPage . '&commentId=' . $commentId . '#comment' . $commentId)->end();
        }

        return [
            'errors' => $errors,
            'text' => $text,
            'name' => $name
        ];
    }

    protected function executeWriteAction(string $writeActionName)
    {
        switch ($writeActionName) {
            case 'post':
                return $this->postComment();
                break;
        }
    }
}
