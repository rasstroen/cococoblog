<?php namespace Blog\Controller\Entry;

use Blog\Classes\Entries\EntriesServiceTrait;
use Blog\Classes\Paging;
use Blog\Controller\Dynamic;
use Blog\Request\RequestTrait;

class Entries extends Dynamic
{
    use EntriesServiceTrait;
    use RequestTrait;

    const ENTRIES_PER_PAGE = 5;

    protected function execute():array
    {
        $currentPage = max(1, $this->getRequest()->getQueryParam('page', 1));
        $perPage = self::ENTRIES_PER_PAGE;
        $totalCount = 0;

        $paging = new Paging();
        $paging->setPage($currentPage);
        $paging->setPerPage($perPage);

        $entries = $this->getEntriesService()->getWithPaging($paging, $totalCount);
        $paging->setTotalItemsCount($totalCount);
        return [
            'entries' => $entries,
            'paging' => $paging
        ];
    }

    protected function getLayout():string
    {
        return 'index';
    }

    protected function getTitle():string
    {
        return 'Blog - entries';
    }
}
