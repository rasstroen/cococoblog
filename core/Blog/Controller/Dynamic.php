<?php namespace Blog\Controller;

use Blog\Classes\CurrentUser\CurrentUserTrait;

abstract class Dynamic extends Base
{
    use CurrentUserTrait;

    protected function beforeAction()
    {
        $this->getCurrentUser()->loadUser();
        parent::beforeAction();
    }
}
