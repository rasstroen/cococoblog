<?php namespace Blog\Controller;

use Blog\Classes\CurrentUser\CurrentUserTrait;
use Blog\Configuration\ProjectConfigurationTrait;
use Blog\Request\RequestTrait;

abstract class Base
{
    use CurrentUserTrait;
    use ProjectConfigurationTrait;
    use RequestTrait;

    /**
     * @var array
     */
    private $response = [];

    /**
     * @var array
     */
    private $writeResponse = [];

    /**
     * @var array
     */
    private $variables = [];

    public function run()
    {
        $writeActionName = $this->getRequest()->getPostParam('writeAction');
        if ($writeActionName) {
            $this->writeResponse = $this->executeWriteAction($writeActionName);
        }
        $this->beforeAction();
        $this->response = $this->execute();
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     */
    public function setVariables(array $variables)
    {
        $this->variables = $variables;
    }

    /**
     * @param string $variableName
     * @param null $defaultValue
     * @return mixed|null
     */
    public function getVariable(string $variableName, $defaultValue = null)
    {
        return array_key_exists($variableName, $this->variables) ? $this->variables[$variableName] : $defaultValue;
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        $templateFileName = $this->getLayoutPath();
        if (is_readable($templateFileName)) {

            $templateVariables = $this->response;
            $templateVariables['_controllerVariables'] = $this->getTemplateVariables();
            $templateVariables['_writeResponse'] = $this->writeResponse;
            $templateVariables['user'] = $this->getCurrentUser()->getUser();
            extract($templateVariables);
            ob_start();
            require_once $templateFileName;
            return ob_get_clean();
        } else {
            throw new \Exception('cant find template' . $templateFileName);
        }
    }

    /**
     * @return array
     */
    private function getTemplateVariables()
    {
        return [
            'title' => $this->getTitle()
        ];
    }

    /**
     * @return string
     */
    private function getLayoutPath()
    {
        return $this->getProjectConfiguration()->get('path.templates') . 'layouts' . DIRECTORY_SEPARATOR . $this->getLayout() . '.php';
    }

    protected function beforeAction()
    {
    }

    protected function executeWriteAction(string $writeActionName)
    {
    }

    abstract protected function execute():array;

    abstract protected function getTitle():string;

    abstract protected function getLayout():string;
}
