<?php namespace Blog\Router;

use Blog\InjectableTrait;
use Blog\Router\Exception\NotFound;

class Router
{
    use InjectableTrait;

    /**
     * @param string $ruleKey
     * @return array
     */
    private function extractRuleKeyAndVariableName(string $ruleKey)
    {
        $variableName = null;
        if (!empty($ruleKey[0]) && '%' == $ruleKey[0] && strlen($ruleKey) > 3) {
            $variableName = substr($ruleKey, 3);
            $ruleKey = substr($ruleKey, 0, 2);
        }
        return [$ruleKey, $variableName];
    }


    public function route(string $relativeUri, array $siteMap, array &$variables = [])
    {

        $urlArray = explode('?', $relativeUri);
        $uri = $urlArray[0];


        $requestUriParts = explode('/', $uri);
        $siteMap = $this->getPageIdByRequestArray($siteMap, $requestUriParts, $variables);

        if (empty($siteMap) || is_array($siteMap)) {
            throw new NotFound('"' . $relativeUri . '" not found');
        }
        return $siteMap;
    }

    /**
     * @param array|string $siteMap
     * @param array $requestUriParts
     * @param array|null $variables
     * @return array|string|callable
     */


    /**
     * @param $siteMap
     * @param array $requestUriParts
     * @param array $variables
     * @return mixed
     */
    private function getPageIdByRequestArray(&$siteMap, array &$requestUriParts, array &$variables = [])
    {
        if (!is_array($siteMap)) {
            return $siteMap;
        }
        $currentRequestUriPart = '';
        while (empty($currentRequestUriPart) && !empty($requestUriParts)) {
            $currentRequestUriPart = array_shift($requestUriParts);
        }

        if (isset($siteMap[$currentRequestUriPart])) {
            return $this->getPageIdByRequestArray($siteMap[$currentRequestUriPart], $requestUriParts, $variables);
        }

        foreach ($siteMap as $siteMapId => $siteMapPart) {
            $variableName = null;
            list($ruleKey, $variableName) = $this->extractRuleKeyAndVariableName($siteMapId);
            if (!$ruleKey) {
                $ruleKey = $siteMapId;
            }

            if (
                ($ruleKey === '%d') && (intval($currentRequestUriPart) . '' === $currentRequestUriPart)
                ||
                ($ruleKey === '%s' && $currentRequestUriPart != '')
            ) {
                if ($variableName) {
                    $variables[$variableName] = $currentRequestUriPart;
                }
                return $this->getPageIdByRequestArray($siteMap[$siteMapId], $requestUriParts, $variables);
            }
        }
    }
}