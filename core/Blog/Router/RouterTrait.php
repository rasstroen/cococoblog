<?php namespace Blog\Router;

trait RouterTrait
{
    /**
     * @return Router
     */
    public function getRouter()
    {
        return Router::instance();
    }
}