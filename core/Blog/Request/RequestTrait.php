<?php namespace Blog\Request;

trait RequestTrait
{
    /**
     * @return Request
     */
    public function getRequest()
    {
        return Request::instance();
    }
}
