<?php namespace Blog\Request;

use Blog\InjectableTrait;

class Request
{
    use InjectableTrait;

    /**
     * @var array
     */
    private $get;

    /**
     * @var array
     */
    private $post;

    /**
     * @var array
     */
    private $outHeaders = [];

    public function prepareRequest()
    {
        $this->get = $_GET ?? [];
        $this->post = $_POST ?? [];
        unset($_GET);
        unset($_POST);
    }

    private function getServerProtocol()
    {
        return $this->server['SERVER_PROTOCOL'] ?? 'HTTP/1.0';
    }

    /**
     * @param $code
     * @param $message
     * @return $this
     */
    public function addHttpHeader(int $code, string $message)
    {
        $this->outHeaders['http'] = [
            'message' => $this->getServerProtocol() . ' ' . $code . ' ' . $message,
            'code' => $code
        ];
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestUri()
    {
        return $_SERVER['REQUEST_URI'] ?? '';
    }

    /**
     * @param $redirectUrl
     * @param int $code
     * @return $this
     */
    public function redirect($redirectUrl, $code = 302)
    {
        if ($redirectUrl && $redirectUrl[0] == '/') {
            $redirectUrl = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $redirectUrl;
        }
        $this->outHeaders['http'] = [
            'message' => 'Location: ' . $redirectUrl,
            'code' => $code
        ];
        return $this;
    }

    /**
     * @param string $paramName
     * @param null $defaultValue
     * @return null
     */
    public function getQueryParam(string $paramName, $defaultValue = null)
    {
        return array_key_exists($paramName, $this->get) ? $this->get[$paramName] : $defaultValue;
    }

    /**
     * @param string $paramName
     * @param null $defaultValue
     * @return null
     */
    public function getPostParam(string $paramName, $defaultValue = null)
    {
        return array_key_exists($paramName, $this->post) ? $this->post[$paramName] : $defaultValue;
    }

    /**
     * @param string $paramName
     * @param null $defaultValue
     * @return null
     */
    public function getSessionParam(string $paramName, $defaultValue = null)
    {
        if (!isset($_SESSION) && !headers_sent()) {
            session_start();
        }
        return array_key_exists($paramName, $_SESSION) ? $_SESSION[$paramName] : $defaultValue;
    }

    /**
     * @param string $paramName
     * @param $value
     * @return $this
     */
    public function setSessionParam(string $paramName, $value)
    {
        if (!isset($_SESSION) && !headers_sent()) {
            session_start();
        }
        $_SESSION[$paramName] = $value;
        return $this;
    }

    /**
     * @return $this
     */
    private function sendHeaders()
    {
        if (!headers_sent()) {
            foreach ($this->outHeaders as $header) {
                header($header['message'], true, $header['code'] ?? null);
            }
        }
        return $this;
    }

    /**
     * @param string $output
     */
    public function end(string $output = '')
    {
        $this->sendHeaders();
        echo $output;
    }
}
