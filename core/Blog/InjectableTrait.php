<?php namespace Blog;

trait InjectableTrait
{
    /**
     * @return mixed
     */
    public static function instance()
    {
        return Injectable::getInstance(static::class);
    }
}
