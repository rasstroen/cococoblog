<?php namespace Blog\Application;

use Blog\Configuration\ProjectConfigurationTrait;
use Blog\Controller\Base;
use Blog\Request\RequestTrait;
use Blog\Router\Exception\Access;
use Blog\Router\Exception\NotFound;
use Blog\Router\RouterTrait;

class Web
{
    use RequestTrait;
    use RouterTrait;
    use ProjectConfigurationTrait;

    /**
     * Web constructor.
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        $this->getProjectConfiguration()->setConfiguration($configuration);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function run()
    {
        /**
         * prepare request
         */
        $this->getRequest()->prepareRequest();

        /**
         * get controller class name from router
         */
        $variables = [];
        try {
            $controllerPageClass = $this->getRouter()->route(
                $this->getRequest()->getRequestUri(),
                $this->getProjectConfiguration()->get('routing.rules', []),
                $variables
            );
            /**
             * put variables from processed request into controller
             */
            $controller = $this->createController($controllerPageClass);
            $controller->setVariables($variables);
            /**
             * run controller
             */
            $controller->run();
            /**
             * "render"
             */
            $html = $controller->render();

            $this->getRequest()->end($html);

        } catch (\Exception $exception) {
            $exception = $exception->getPrevious() ?? $exception;
            switch (get_class($exception)) {
                case NotFound::class:
                    $this->getRequest()->addHttpHeader(404, $exception->getMessage())->end('Page Not Found');
                    break;
                case Access::class:
                    $this->getRequest()->addHttpHeader(403, 'Forbidden')->end('Access Denied');
                    break;
                default:
                    $this->getRequest()->addHttpHeader(502, 'Error')->end('Internal Server Error');
                    throw $exception;
                    break;
            }
        }
    }

    /**
     * @param string $controllerPageClass
     * @return Base
     */
    private function createController(string $controllerPageClass)
    {
        return new $controllerPageClass;
    }
}
