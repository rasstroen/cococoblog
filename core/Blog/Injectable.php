<?php namespace Blog;

class Injectable
{
    private static $instances = [];

    /**
     * @param string $class
     * @return mixed
     */
    public static function getInstance(string $class)
    {
        return isset(self::$instances[$class]) ? self::$instances[$class] : self::$instances[$class] = new $class();
    }
}
