<?php namespace Blog\Database;

trait DbConnectionTrait
{
    /**
     * @return Database
     */
    protected function getDb()
    {
        return Database::instance();
    }
}
