<?php namespace Blog\Database;

class Connection
{
    private $connection;

    /**
     * Connection constructor.
     * @param \mysqli $connection
     */
    public function __construct(\mysqli $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->connection->insert_id;
    }

    /**
     * @param string $query
     * @return bool|\mysqli_result
     * @throws \Exception
     */
    public function query(string $query)
    {
        $result = @$this->connection->query($query);
        if ($this->connection->errno) {
            throw new \Exception($this->connection->error);
        }
        return $result;
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function escape(string $value):string
    {
        return mysqli_real_escape_string($this->connection, $value);
    }

    /**
     * @param string $query
     * @return array
     * @throws \Exception
     */
    public function selectAll(string $query)
    {
        $result = @$this->connection->query($query);
        if ($this->connection->errno) {
            throw new \Exception($this->connection->error);
        }
        $out = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $out[] = $row;
        }
        return $out;
    }

    /**
     * @param $query
     * @return mixed|null
     * @throws \Exception
     */
    public function selectRow($query)
    {
        $result = $this->selectAll($query);
        return $result ? reset($result) : null;
    }

    /**
     * @param $query
     * @return null
     * @throws \Exception
     */
    public function selectSingle($query)
    {
        $result = $this->selectAll($query);
        $row = $result ? reset($result) : null;
        return $row ? reset($row) : null;
    }
}