<?php namespace Blog\Database;

use Blog\Configuration\ProjectConfigurationTrait;
use Blog\InjectableTrait;

class Database
{
    use InjectableTrait;
    use ProjectConfigurationTrait;
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @return Connection
     * @throws \Exception
     */
    public function getConnection()
    {
        if (null === $this->connection) {
            $this->connection = $this->connect();
        }
        return $this->connection;
    }

    /**
     * @return Connection
     * @throws \Exception
     */
    private function connect()
    {
        $connection = @new \mysqli(
            $this->getProjectConfiguration()->get('mysql.host', 'localhost'),
            $this->getProjectConfiguration()->get('mysql.user'),
            $this->getProjectConfiguration()->get('mysql.password'),
            $this->getProjectConfiguration()->get('mysql.database'),
            $this->getProjectConfiguration()->get('mysql.port', '3306')
        );
        if ($connection->connect_errno) {
            throw new \Exception($connection->connect_error . ' [#' . $connection->connect_errno . ']');
        }
        return new Connection($connection);
    }
}