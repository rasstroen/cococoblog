<?php
/**
 * @var \Blog\Classes\Entries\Entry[] $entries
 */
require 'html/head.php';
?>
<body>
<div class="header">
    <?php require 'html/userplank.php'; ?>
</div>
<div class="content">
    <ul class="entries-list">
        <?php foreach ($entries as $entry) {
            $username = $entry->getAuthor() ? $entry->getAuthor()->getUsername() : "аноним";
            ?>
            <li class="entry">
                <span class="username"><?= htmlspecialchars($username) ?></span>
                <span class="date">написал <?= htmlspecialchars($entry->getFormattedDate()) ?></span>
                <h3><a href="<?= $entry->getUrl() ?>"><?php echo htmlspecialchars($entry->getTitle()) ?></a></h3>
                <div class="content">
                    <?= htmlspecialchars($entry->getShortText(30)); ?>
                </div>
                <div>
                    <a href="<?= $entry->getCommentsUrl() ?>">comments(<?= (int) $entry->getCommentsCount(); ?>)</a>
                </div>
            </li>
        <?php } ?>
    </ul>
    <?php require __DIR__ . '/../helpers/paging.php'; ?>
</div>
<div class="footer"></div>
</body>
