<?php
/**
 * @var \Blog\Classes\Entries\Entry[] $entries
 */
require 'html/head.php';
?>
<body>
<div class="header">
    <?php require 'html/userplank_nologin.php'; ?>
</div>
<div class="content">
    <div class="login">
        <?php
        if (isset($_writeResponse['errors'])) {
            foreach ($_writeResponse['errors'] as $error) {
                ?><span class="error"><?= htmlspecialchars($error); ?></span><?php
            }
        } ?>
        <?php
        if (empty($user)) {
            ?>
            <h1>Login</h1>
            <span class="register-form">
            <form method="post" action="/login">
                <div class="field"><span>login:</span><input name="username"
                                                             value="<?= isset($_writeResponse['username']) ? $_writeResponse['username'] : ''; ?>">
                </div>
                <div class="field"><span>password:</span><input name="password" type="password"></div>
                <div>
                    <input type="submit" name="writeAction" value="login">
                    <input type="submit" name="writeAction" value="register">
                </div>
            </form>
        </span>
            <?php
        } else {
            ?>
            <span class="registered">
                <span>You are logged in as <b><?= htmlspecialchars($user->getUsername()) ?></b>.</span>
                <span>
                    Now you can <a href="/entries/post">write a post</a>.
                </span>
                 <form method="post" action="/login">
                     <input type="submit" name="writeAction" value="logout">
                 </form>
            </span>
            <?php
        }
        ?></div>
</div>
<div class="footer"></div>
</body>
