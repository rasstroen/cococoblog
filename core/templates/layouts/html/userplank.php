<?php
/**
 * @var \Blog\Classes\User\User $user
 */
?>
<div class="plank">
    <h1><a href="/">Blog</a></h1>
    <?php
    if (empty($user)) {
        ?>
        <span class="register-form">
            <form method="post" action="/login">
                <div><span>login:</span><input name="username"></div>
                <div><span>password:</span><input name="password" type="password"></div>
                <div>
                    <input type="submit" name="writeAction" value="login">
                    <input type="submit" name="writeAction" value="register">
                </div>
            </form>
        </span>
        <?php
    } else {
        ?>
        <span class="registered">
             <form method="post" action="/login">
                 <input type="submit" name="writeAction" value="logout">
             </form>
            <span>current user: <b><?= htmlspecialchars($user->getUsername()) ?></b></span>
            <span><a href="/entries/post">write a post</a></span>
        </span>
        <?php
    }
    ?>
</div>
