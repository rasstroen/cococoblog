<?php
/**
 * @var array $_controllerVariables
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo htmlspecialchars($_controllerVariables['title']); ?></title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="/static/css/project.css">
</head>