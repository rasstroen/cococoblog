<?php
/**
 * @var \Blog\Classes\Entries\Entry $entry
 * @var array $_writeResponse
 */

$formData = [];
if (!empty($_writeResponse)) {
    $formData = [
        'title' => $_writeResponse['title'],
        'text' => $_writeResponse['text']
    ];
} else {
    if (isset($entry)) {
        $formData = [
            'title' => $entry->getTitle(),
            'text' => $entry->getText(),
        ];
    }
}
require 'html/head.php';
?>
<body>
<div class="header">
    <?php require 'html/userplank.php'; ?>
</div>
<div class="content">
    <form method="post" class="post-form">
        <?php
        if (isset($_writeResponse['errors'])) {
            foreach ($_writeResponse['errors'] as $error) {
                ?><span class="error"><?= htmlspecialchars($error); ?></span><?php
            }
        } ?>
        <div>
            <span>title:</span><input name="title"
                                      value="<?= isset($formData['title']) ? htmlspecialchars($formData['title']) : '' ?>">
        </div>
        <div>
            <span>text:</span><textarea
                name="text"><?= isset($formData['text']) ? htmlspecialchars($formData['text']) : '' ?></textarea>
        </div>
        <div>
            <input type="submit" name="writeAction" value="post">
            <?php if (isset($entry)) { ?>
                <input type="submit" name="writeAction" value="delete">
            <?php } ?>
        </div>
        <?php if (isset($entry)) { ?>
            <input type="hidden" name="id" value="<?= (int) $entry->getId(); ?>">
        <?php } ?>
    </form>
</div>
<div class="footer"></div>
</body>
