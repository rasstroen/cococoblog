<?php
/**
 * @var \Blog\Classes\Entries\Entry $entry
 * @var array $_writeResponse
 * @var \Blog\Classes\User\User $user
 * @var \Blog\Classes\Comments\Comment[] $comments
 * @var \Blog\Classes\Paging $paging
 * @var int $commentId
 */
require 'html/head.php';
$formData = [];
if (!empty($_writeResponse)) {
    $formData = [
        'name' => $_writeResponse['name'],
        'text' => $_writeResponse['text']
    ];
} else {
    $formData = [
        'name' => $user ? $user->getUsername() : 'anonymous',
        'text' => '',
    ];
}
$username = $entry->getAuthor() ? $entry->getAuthor()->getUsername() : "аноним";
?>
<body>
<div class="header">
    <?php require 'html/userplank.php'; ?>
</div>
<div class="content">
    <ul class="entries-item">
        <li class="entry">
            <span class="username"><?= htmlspecialchars($username) ?></span>
            <span class="date">wrote at <?= htmlspecialchars($entry->getFormattedDate()) ?>
                <?php if ($user && ($user->getId() == $entry->getAuthorId())) { ?>
                    <a href="<?= $entry->getEditUrl() ?>">edit</a>
                <?php } ?>
            </span>
            <h1><?php echo wordwrap(htmlspecialchars($entry->getTitle()), 35, " ", true) ?></h1>
            <div class="content">
                <?= nl2br(htmlspecialchars($entry->getText())); ?>
            </div>

        </li>
    </ul>
    <div class="comments" id="comments"> Comments (<?= (int) $paging->getTotalCount() ?>):
        <ul>
            <?php foreach ($comments as $comment) {
                ?>
                <li class="comment-item<?php if ($commentId == $comment->getId()) {
                    echo " selected";
                } ?>">
                    <span id="comment<?= (int) $comment->getId() ?>"
                          class="username"><?= htmlspecialchars($comment->getName()) ?><?php if (!$comment->getAuthorId()) {
                            echo " <span class='anonymous'>(anonymous)</span>";
                        } ?></span>
                    <span class="date">wrote at <?= htmlspecialchars($comment->getFormattedDate()) ?></span>
                    <div class="content">
                        <?= wordwrap(htmlspecialchars($comment->getText()), 60, "\n", true) ?>
                    </div>
                </li>
                <?php
            } ?>
            <li>
                <?php require __DIR__ . '/../helpers/paging.php'; ?>
            </li>
            <li class="add">
                <div>post new comment:</div>
                <form method="post" class="post-comment-form">
                    <?php
                    if (isset($_writeResponse['errors'])) {
                        foreach ($_writeResponse['errors'] as $error) {
                            ?><span class="error"><?= htmlspecialchars($error); ?></span><?php
                        }
                    } ?>
                    <div>
                        <span>your name:</span>
                        <input name="name" <?php if ($user) echo ' disabled="disabled" ' ?>
                               value="<?= isset($formData['name']) ? htmlspecialchars($formData['name']) : '' ?>">
                    </div>
                    <div>
                        <span>text:</span><textarea
                            name="text"><?= isset($formData['text']) ? htmlspecialchars($formData['text']) : '' ?></textarea>
                    </div>
                    <div>
                        <input type="submit" name="writeAction" value="post">
                    </div>
                    <input type="hidden" name="entry_id" value="<?= (int) $entry->getId(); ?>">
                </form>
            </li>
        </ul>

    </div>


</div>
<div class="footer"></div>
</body>
