<?php
/**
 * @var \Blog\Classes\Paging $paging
 */
$pages = $paging->getPrettyPages();
$pagerLinks = [];
$currentPage = $paging->getCurrentPage();
foreach ($pages as $page) {
    $pagerLinks[$page] = ['link' => '?page=' . $page, 'id' => $page];
    if ($page == $currentPage) {
        $pagerLinks[$page]['current'] = true;
    }
}
$previousPage = 0;
?>

<ul class="paging">
    <?php foreach ($pagerLinks as $page) {
        if ($previousPage != $page['id'] - 1) {
            ?><span class="delimiter">...</span><?php
        }
        ?>
        <li><a <?php if (isset($page['current'])){ ?>class="current"
               <?php } ?>href="<?= $page['link']; ?>"><?= $page['id']; ?></a>
        </li>
        <?php
        $previousPage = $page['id'];
    }
    ?>
</ul>
