<?php


setlocale(LC_TIME, "ru_RU.UTF-8");
date_default_timezone_set('Etc/GMT-3');
session_cache_limiter('public');
ini_set('display_errors', false);
error_reporting(E_ALL);

$rootPath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'core/';

set_include_path(get_include_path() . PATH_SEPARATOR . $rootPath);
require_once $rootPath . 'Autoloader.php';

$application = new \Blog\Application\Web(
    require_once $rootPath . 'config/project.php'
);

$application->run();
